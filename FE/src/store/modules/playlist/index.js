import API from '../../base/';

export default {
    namespaced: true,
    state: {
        playlists: {},
    },
    getters: {},
    mutations: {
        SET_PLAYLISTS(state, data) {
            state.playlists = data;
        },
    },
    actions: {
        async savePlaylist({ commit }, payload) {
            const res = await API.post('/playlists', payload)
                .then((res) => {
                    return res;
                })
                .catch((err) => {
                    return err.response;
                });

            return res;
        },
        async saveSong({ commit }, payload) {
            const res = await API.post('songs', payload)
                .then((res) => {
                    return res;
                })
                .catch((err) => {
                    return err.response;
                });

            return res;
        },
        async getPlaylists({ commit }, data) {
            const res = await API.get('playlists', data)
                .then((res) => {
                    commit('SET_PLAYLISTS', res.data);
                    return res;
                })
                .catch((err) => {
                    return err.response;
                });

            return res;
        },
        async updatePlaylist({ commit }, data) {
            const res = await API.put(`playlists/${data.id}`, data)
                .then((res) => {
                    commit('SET_PLAYLIST', data);
                    return res;
                })
                .catch((err) => {
                    return err.response;
                });

            return res;
        },

        async deletePlaylist({ commit }, id) {
            const res = await API.delete(`playlists/${id}`)
                .then((res) => {
                    return res;
                })
                .catch((err) => {
                    return err.response;
                });

            return res;
        },

        /** END OF ACTION */
    },
};