<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Song;
use App\Models\PlaylistSong;

class PlaylistSongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $song = PlaylistSong::with(['song', 'playlist'])->where('id', $id)->get()->all();

        return response()->json($song);
    }

    public function playlist($id)
    {
        $song_id = PlaylistSong::find('playlist_id', $id);

        if(!empty($song_id)){
            return response()->json(PlaylistSong::with('song', 'playlist')->get());
        } else {
            return response()->json('Playlist not found.');
        }
        return response()->json($song);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addToPlaylist(Request $request, $id)
    {
        $song = Song::findOrFail($id);

        $addsong = PlaylistSong::create([
            'song_id' => $song->id,
            'playlist_id' => $request->playlist_id,
        ]);

        return response()->json($addsong);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = PlaylistSong::destroy($id);
        return response()->json($delete);
    }
}
