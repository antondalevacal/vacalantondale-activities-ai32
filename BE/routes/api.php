<?php

use App\Http\Controllers\PlaylistSongController;
use App\Http\Controllers\PlaylistController;
use App\Http\Controllers\SongController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('songs', SongController::class);
Route::apiResource('playlists', PlaylistController::class);
Route::apiResource('playlistsongs', PlaylistSongController::class);
Route::get('playlistsongs/{id}', [PlaylistSongController::class, 'playlist']);
Route::post('addtoplaylist/{id}', [PlaylistSongController::class, 'addToPlaylist']);
Route::post('uploadSong', [SongController::class, 'uploadSong']);